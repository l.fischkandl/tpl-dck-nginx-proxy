### Nginx Proxy Template

Auf Basis von [jwilder](https://github.com/jwilder/nginx-proxy).

In der vhost.d/localhost werden die Routings für die Container gesetzt.         
Im Docker Netzwerk sind die Container über http://containername:port zu erreichen. 